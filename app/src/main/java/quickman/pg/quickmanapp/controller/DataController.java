package quickman.pg.quickmanapp.controller;

import quickman.pg.quickmanapp.model.Drug;
import quickman.pg.quickmanapp.model.Food;
import quickman.pg.quickmanapp.model.Measurement;
import quickman.pg.quickmanapp.model.Patient;

public class DataController {

	private QuickManController quickManController;

	public DataController(QuickManController quickManController){
		this.quickManController = quickManController;
	}
	public void addMeasurement(Measurement measurement) {
		Patient patient = this.quickManController.getPatient();
		patient.addMeasurement(measurement);
		
	}

	public void addFood(Food food) {
		Patient patient = this.quickManController.getPatient();
		patient.addFood(food);
	}

	public void addDrug(Drug drug) {
		Patient patient = this.quickManController.getPatient();
		patient.addDrug(drug);
	}

}
