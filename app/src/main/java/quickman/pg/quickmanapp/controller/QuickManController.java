package quickman.pg.quickmanapp.controller;

import quickman.pg.quickmanapp.model.Patient;

public class QuickManController {

	private IOController iOController;

	private Patient patient;

	private DataController medicalDataController;

	private MedicalController medicalController;

	
	public QuickManController(){
		this.iOController = new IOController();
		this.medicalController = new MedicalController(this);
		this.medicalDataController = new DataController(this);
	}
	public void setPatient(Patient patient){
		this.patient = patient;
	}
	public Patient getPatient(){
		return this.patient;
	}
	public IOController getIOController() {
		return iOController;
	}
	public DataController getMedicalDataController() {
		return medicalDataController;
	}
	public MedicalController getMedicalController() {
		return medicalController;
	}
	
	
	
}
