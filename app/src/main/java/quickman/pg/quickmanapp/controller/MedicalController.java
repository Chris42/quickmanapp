package quickman.pg.quickmanapp.controller;

import quickman.pg.quickmanapp.model.MedicalData;
import quickman.pg.quickmanapp.model.Patient;

public class MedicalController {

	private QuickManController quickManController;

	public MedicalController(QuickManController quickManController){
		this.quickManController = quickManController;
	}
	
	public void changeWeight(int weight) {
		
		MedicalData medicalData = getMedicalData();
		medicalData.setWeight(weight);
	}

	public void changeHeight(int height) {
		MedicalData medicalData = getMedicalData();
		medicalData.setHeight(height);
	}

	public void changeINRRangeMin(double inrRangeMin) {
		MedicalData medicalData = getMedicalData();
		medicalData.setInrRangeMin(inrRangeMin);
	}

	public void changeINRRangeMax(double inrRangeMax) {
		MedicalData medicalData = getMedicalData();
		medicalData.setInrRangeMax(inrRangeMax);
	}
	
	private MedicalData getMedicalData(){
		Patient patient = this.quickManController.getPatient();
		return patient.getMedicalData();
	}

}
