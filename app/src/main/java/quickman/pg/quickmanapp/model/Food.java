package quickman.pg.quickmanapp.model;

import java.util.Date;

public class Food {

	private Date date;
	
	private String name;

	private int quantity;

	private int quantityVitaminK;

	

	public Food(Date date, String name, int quantity, int quantityVitaminK){
		this.name = name;
		this.quantity = quantity;
		this.quantityVitaminK = quantityVitaminK;
		this.date = date;
	}
	
	public String getName() {
		return name;
	}

	public int getQuantity() {
		return quantity;
	}

	public int getQuantityVitaminK() {
		return quantityVitaminK;
	}

	public Date getDate() {
		return date;
	}
	
}
