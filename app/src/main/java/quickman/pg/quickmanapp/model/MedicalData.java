package quickman.pg.quickmanapp.model;

public class MedicalData {

	private int height;

	private int weight;

	private double inrRangeMin;
	
	private double inrRangeMax;


	public MedicalData(int height, int weight, double inrRangeMin, double inrRangeMax){
		this.height = height;
		this.weight = weight;
		this.inrRangeMin = inrRangeMin;
		this.inrRangeMax = inrRangeMax;
	}
	
	//Getter and Setter for all attributes
	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

	public double getInrRangeMax() {
		return inrRangeMax;
	}

	public void setInrRangeMax(double inrRangeMax) {
		this.inrRangeMax = inrRangeMax;
	}

	public double getInrRangeMin() {
		return inrRangeMin;
	}

	public void setInrRangeMin(double inrRangeMin) {
		this.inrRangeMin = inrRangeMin;
	}

	
	
}
