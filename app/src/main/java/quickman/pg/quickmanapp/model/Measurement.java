package quickman.pg.quickmanapp.model;

import java.util.Date;

public class Measurement {

	private Date date;

	private double inrValue;

	public Measurement(Date date, double inrValue){
		this.date = date;
		this.inrValue = inrValue;
	}
	
	public Date getDate() {
		return date;
	}

	public double getInrValue() {
		return inrValue;
	}

	
	
}
