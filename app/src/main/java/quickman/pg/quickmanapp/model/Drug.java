package quickman.pg.quickmanapp.model;

import java.util.Date;

public class Drug {

	private Date date;
	
	private String name;

	
	public Drug(Date date, String name){
		this.date = date;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public Date getDate() {
		return date;
	}
}
