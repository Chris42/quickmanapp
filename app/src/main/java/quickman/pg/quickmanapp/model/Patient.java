package quickman.pg.quickmanapp.model;

import java.util.ArrayList;
import java.util.Collection;

public class Patient {

	private int id;

	private String lastName;

	private String firstName;

	private int age;

	private Collection<Measurement> measurements;

	private Collection<Drug> medication;

	private Collection<Food> food;

	private MedicalData medicalData;

	
	/**
	 * Constructor for the class Patient with all parameters
	 * @param id
	 * @param lastName
	 * @param firstName
	 * @param age
	 */
	public Patient(int id, String lastName, String firstName, int age, MedicalData medicalData){
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.age = age;
		this.measurements = new ArrayList<Measurement>();
		this.food = new ArrayList<Food>();
		this.medication = new ArrayList<Drug>();
		this.medicalData = medicalData;
		
	}
	
	
	//Method for adding value to the lists
	public void addMeasurement(Measurement newMeasurement){
		this.measurements.add(newMeasurement);
	}
	public void addDrug(Drug newDrug){
		this.medication.add(newDrug);
	}
	public void addFood(Food newFood){
		this.food.add(newFood);
	}
	//Getter for the lists
	public Collection<Measurement> getMeasurements() {
		return measurements;
	}


	public Collection<Drug> getMedication() {
		return medication;
	}


	public Collection<Food> getFood() {
		return food;
	}
	
	//Getter and Setter for all attributes
	public String getLastName() {
		return lastName;
	}


	public String getFirstName() {
		return firstName;
	}


	public int getAge() {
		return age;
	}


	public MedicalData getMedicalData() {
		return medicalData;
	}

	public void setMedicalData(MedicalData medicalData) {
		this.medicalData = medicalData;
	}

	public int getId() {
		return id;
	}
	

	
}
