package quickman.pg.quickmanapp.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import quickman.pg.quickmanapp.R;
import quickman.pg.quickmanapp.controller.QuickManController;
import quickman.pg.quickmanapp.model.Patient;

public class MainActivity extends AppCompatActivity {

    private QuickManController qmc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        qmc = new QuickManController();
        Patient patient = qmc.getIOController().load();
        qmc.setPatient(patient);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Patient patient = qmc.getPatient();
        TextView nameTextView = (TextView) findViewById(R.id.patientName);
        nameTextView.setText("Patient: " + patient.getFirstName() +" "+ patient.getLastName());


    }



    public void openOverView(View view){
        /*Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE,message);
        startActivity(intent);*/
        System.out.println("Overview");
    }
}
